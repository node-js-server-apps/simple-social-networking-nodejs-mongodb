const jwt = require('jsonwebtoken');
const User = require("../models/users")
const UserService = require('../services/user-service')
const fs = require("fs");
var publicKey = fs.readFileSync('./keys/jwtRS256.key.pub', 'utf8');
exports.verifyJwt = async (req, res, next) => {
    let authHeader = req.headers['authorization'];
    if (authHeader) {
        try {
            let authorization = authHeader.split(' ');
            if (authorization[0] !== 'Bearer') {
                res.status(403).json({message: 'bearer auth token is required to access resource'});
            } else {
                var verifyOptions = {
                    algorithms: ["RS256"]
                };
                let verify = await jwt.verify(authorization[1], publicKey, verifyOptions);
                if (verify) {
                    req.user = await UserService.getUser(verify.username);
                    next();
                } else {
                    res.status(403).json({message: 'invalid jwt token'});
                }
            }
        } catch (e) {
            console.error(e);
            res.status(403).json({message: 'invalid auth header'});
        }
    } else {
        res.status(403).json({message: 'login to continue'});
    }
};
