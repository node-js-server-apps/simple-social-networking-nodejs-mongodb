const tagService = require('../services/tag-service')


exports.resolveTags = async (req, res, next) => {
    let tweetTags = req.body.tags;
    if(tweetTags) {
        tweetTags = tweetTags.map(function (x) {
            x = x.toLowerCase();
            x = x.trim();
            x = x.replace(/\s/g, "");
            return x;
        });

        console.log(tweetTags);
        const dbTags = [];
        for (let i = 0; i < tweetTags.length; i++) {
            const newTag = await tagService.createTag(req.user, tweetTags[i]);
            dbTags.push(newTag);
        }
        console.log(dbTags);
        req.body.tags = dbTags;
    }
    next();
};