var express = require('express');
const authController = require("../controllers/auth");
var router = express.Router();
const {body} = require('express-validator')

/**
 * user Authentication router and regerence controller
 */
const loginValidator = body('username').trim().isLength({
    min: 3,
    max: 12
}).withMessage("Please enter username with min 6 char and max 12 char long")

const registerValidator = [
    body('username').trim().isLength({
        min: 3,
        max: 12
    }).withMessage("Please enter username with min 6 char and max 12 char long"),
    body('password').trim().isLength({
        min: 6,
        max: 24
    }).withMessage("Please enter password"),
    body('name').trim().isLength({
        min: 3,
        max: 24
    }).withMessage("Please enter name")]
router.post('/login', [loginValidator], authController.login);

router.post('/register', registerValidator, authController.register);

module.exports = router;
