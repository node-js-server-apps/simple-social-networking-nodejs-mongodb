var mongo = require('mongodb');

const User = require('../models/users')
const upload = require('../image-uploaders/user-images-uploader-new');
const mongoose = require('mongoose');
const userService = require("../services/user-service");

exports.getMe = async (req, res, next) => {
    try {
        const me = await User.findOne({username: req.user.username}, {id: 1, username: 1, name: 1, mediaId: 1})
        res.status(200).json(me)
    } catch (e) {
        res.status(500).json({message: 'Error getting my details users'})
    }
};


exports.updateMe = async (req, res, next) => {
    try {
        const name = req.body.name;
        const me = await User.findOneAndUpdate({username: req.user.username}, {name: name});
        res.status(200).json(me)
    } catch (e) {
        res.status(500).json({message: 'Error getting my details users'})
    }
};

exports.getUsers = async (req, res, next) => {
    try {
        const allUsers = await User.find({}, {username: 1, name: 1})
        res.status(200).json({users: allUsers})
    } catch (e) {
        res.status(500).json({message: 'Error getting all users'})
    }
}

exports.uploadProfileImage = async (req, res, next) => {
    try {
        const responseImageUpload = await upload(req, res);
        console.log(req.file);
        if (req.file === undefined) {
            return res.status(422).json({message: 'You must select a file.', upload: ''});
        }
        const user = await userService.updateProfileDetails(req.user, req.file.id);
        return res.status(200).json({message: 'File has been uploaded.', upload: req.file.id});
    } catch (error) {
        console.log(error);
        return res.status(500).json({message: 'Error when trying upload image: ${error}', upload: ''});
    }
};

exports.profileImages = async (req, res, next) => {
    // const gfs = new mongoose.mongo.GridFSBucket('mongodb://172.17.0.2:27017/simple-node-js', {bucketName: 'fs'});
    var gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db, {bucketName: process.env.USER_IMAGES_BUCKET});

    gfs.find().toArray((err, files) => {
        //check if files exist
        if (!files || files.length == 0) {
            return res.status(404).json({
                err: "No files exist"
            })
        }
        // files exist
        return res.json(files)
    })

};

exports.profileImagesById = async (req, res, next) => {
    const photoId = req.params.photoId;
    const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db, {bucketName: process.env.USER_IMAGES_BUCKET});
    const downStream = gfs.openDownloadStream(new mongoose.Types.ObjectId(photoId));
    return downStream.pipe(res);
};

exports.count = async (req, res) => {
    try {
        const count = await userService.count();
        return res.status(200).json({count: count});
    } catch (e) {
        return res.status(500).json({message: 'error getting all tags count'});
    }
};

exports.suggestions = async (req, res) => {
    try {
        const suggestions = await userService.getSuggestionList();
        return res.status(200).json({users: suggestions});
    } catch (e) {
        return res.status(500).json({message: 'error getting all suggestions count'});
    }
};

