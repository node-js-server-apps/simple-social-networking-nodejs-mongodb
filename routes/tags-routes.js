var express = require('express');
const {body} = require('express-validator')
const tagsController = require("../controllers/tags");
const jwtAuth = require('../middlewares/jwt_verify');
var router = express.Router();

const tagValidator = body('tagName').trim().isLength({min: 4});
router.get('/recent', tagsController.getTop5Tags);
router.get('/', jwtAuth.verifyJwt, tagsController.getTags);
router.get('/count', tagsController.count);
router.post('/', [tagValidator], jwtAuth.verifyJwt, tagsController.createTag);

module.exports = router;
