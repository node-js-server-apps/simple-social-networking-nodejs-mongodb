const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userConnection = new Schema({
    user: {type: Schema.Types.ObjectId, ref: "User", required: true},
    followers: [{type: Schema.Types.ObjectId, ref: "User", required: false}],
    followings: [{type: Schema.Types.ObjectId, ref: "User", required: false}],
})

userConnection.set('toJSON', {
    virtuals: true,
    versionKey:false,
    transform: function (doc, ret) {   delete ret._id  }
});

module.exports = mongoose.model('UserConnection', userConnection);