const util = require("util");
const multer = require("multer");
const {GridFsStorage} = require("multer-gridfs-storage");
const mongoose = require("mongoose");

var storage = new GridFsStorage({
    url: process.env.DB_CONNECTION_STRING,
    options: {useNewUrlParser: true, useUnifiedTopology: true},
    file: (req, file) => {
        const match = ["image/png", "image/jpeg"];
        const id = new mongoose.Types.ObjectId();
        const filename = `${Date.now() - id}--${file.originalname}`;
        if (match.indexOf(file.mimetype) === -1) {
            return {
                bucketName: process.env.TWEET_IMAGES_BUCKET,
                filename: filename,
                metadata: {user: req.user, tweet: req.tweetId},
            };
        }

        return {
            bucketName: process.env.TWEET_IMAGES_BUCKET,
            metadata: {user: req.user, tweet: req.tweetId},
            filename: filename
        };
    }
});

const uploadFile = multer({storage: storage}).single("file");
const uploadTweetImageFilesMiddleware = util.promisify(uploadFile);
module.exports = uploadTweetImageFilesMiddleware;