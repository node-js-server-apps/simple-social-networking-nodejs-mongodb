const Tweet = require('../models/tweets')
const userConnectionService = require('../services/user-connection-service')
const mongoose = require("mongoose");

exports.findOne = async (user, id) => {
    return Tweet.findOne({created_by: user, _id: new mongoose.Types.ObjectId(id)});
};

exports.findOneById = async (id) => {
    return Tweet.findById(id);
};

exports.getRecentTweets = async () => {
    return Tweet.find()
        .populate('created_by', '_id name username mediaId')
        .populate('tags', '_id name')
        .sort({'createdAt': -1});
}
exports.createTweet = async (user, content, tags) => {
    console.log(tags);
    let newTweet = new Tweet({content: content, created_by: user, tags: tags, replies: []})
    return await newTweet.save();
};

exports.deleteTweet = async (user, tweetId) => {
    return Tweet.findOneAndDelete({created_by: user, _id: new mongoose.Types.ObjectId(tweetId)});
}

exports.getMyTweets = async (user) => {
    return Tweet.find({created_by: user})
        .populate('tags', 'name')
        .populate('created_by', '_id name username mediaId')
        .sort({'createdAt': -1});
};

exports.getMyTimeline = async (user) => {
    const myConnection = await userConnectionService.getUserConnectionFollowings(user);
    return Tweet.find({created_by: {$in: myConnection.followings}})
        .populate('tags', '_id name')
        .populate('created_by', '_id name username mediaId')
        .sort({'createdAt': -1});
}

exports.createReply = async (user, id, content) => {
    const newReplyId = mongoose.Types.ObjectId();
    const newReply = {
        user_id: user._id,
        name: user.name,
        username: user.username,
        content: content,
        createdAt: Date.now(),
        _id: newReplyId
    };
    try {
        const tweet = await Tweet.findById(id);
        tweet.replies.push(newReply);
        await tweet.save();
        return newReply;
    } catch (e) {
        console.log(e);
        return null;
    }
};

exports.deleteReply = async (user, tweetId, replyId) => {
    try {
        const tweet = await Tweet.findById(tweetId);
        const newFilteredReplies = tweet.replies.filter(x => {
            const validReply = x._id.equals(replyId);
            const validUser = user._id.equals(x.user_id);
            return !(validReply && validUser);
        });
        tweet.replies = newFilteredReplies;
        await tweet.save();
        return {deletedReplyId: replyId};
    } catch (e) {
        console.log(e);
        return null;
    }
};

exports.getMyTweetsCount = async (user) => {
    return Tweet.find({created_by: user}).count();
};

exports.count = async () => {
    return Tweet.find({}).count();
};

exports.like = async (user, tweetId) => {
    try {
        const tweet = await Tweet.findById(tweetId);
        tweet.likes.addToSet(user);
        await tweet.save();
        return {message: 'liked tweet ' + tweetId};
    } catch (e) {
        console.log(e);
        return null;
    }
};


exports.unlike = async (user, tweetId) => {
    try {
        const tweet = await Tweet.findById(tweetId);
        const unliked = tweet.likes.pull(user);
        await tweet.save();
        return {message: 'unliked tweet ' + tweetId};
    } catch (e) {
        console.log(e);
        return null;
    }
};

exports.storeFileDataToTweetObject = async (user, tweetId, file) => {
    try {
        const update = await Tweet.updateOne({"_id": tweetId}, {$set: {"media": [file.id]}});
        return true;
    } catch (e) {
        console.log('error storing file in the database');
        return false;
    }
};
