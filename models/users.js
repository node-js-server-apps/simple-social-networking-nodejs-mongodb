const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt')

function encryptPassword(password) {
    return bcrypt.hashSync(password, 12);
}

const userSchema = new Schema({
    username: {type: String, required: true},
    password: {type: String, required: true, set: encryptPassword},
    name: {type: String, required: true},
    mediaId: {type: String},
})

userSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        delete ret._id;
        delete ret.password
    }
});
module.exports = mongoose.model('User', userSchema);