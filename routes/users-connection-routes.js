const express = require('express');
const userConnectionController = require("../controllers/users-connections");
const jwtVerify = require("../middlewares/jwt_verify")
const {body} = require("express-validator");
const router = express.Router();
const userService = require('../services/user-service')

const targetUserValidator = [body('targetUser').trim().isLength({min: 24})]

router.get('/', jwtVerify.verifyJwt, userConnectionController.getMyConnections);
router.get('/:id', jwtVerify.verifyJwt, userConnectionController.isFollowing);
router.post('/follow', targetUserValidator, userService.exists, jwtVerify.verifyJwt, userConnectionController.followUser);
router.post('/unfollow', targetUserValidator, userService.exists, jwtVerify.verifyJwt, userConnectionController.unFollowUser);
router.get('/me/suggestions', jwtVerify.verifyJwt, userConnectionController.getSuggestions);
module.exports = router;
