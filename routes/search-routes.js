var express = require('express');
const {body} = require('express-validator')
const searchController = require("../controllers/search");
var router = express.Router();


router.get('/tags', searchController.searchTweetsByTags);
router.post('/text', searchController.searchTweetsByText);

module.exports = router;
