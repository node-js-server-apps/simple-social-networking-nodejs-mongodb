const {validationResult} = require('express-validator');
const tweetService = require('../services/tweet-service')
const tweetImageUploader = require('../image-uploaders/tweet-images-uploader')
const mongoose = require("mongoose");
exports.getTweets = async (req, res, next) => {
    try {
        const recentTweets = await tweetService.getRecentTweets();
        res.status(200).json({message: 'success', tweets: recentTweets, size: recentTweets.length});
    } catch (e) {
        console.log(e);
        res.status(500).json({message: 'unable to get recent tweets'});
    }
};

exports.getMyTweets = async (req, res, next) => {
    try {
        const allPosts = await tweetService.getMyTweets(req.user, req.query.tags);
        res.status(200).json({message: 'success', tweets: allPosts, size: allPosts.length});
    } catch (err) {
        console.log('error is: ' + err);
        res.status(500).json({message: 'unable to get my tweets'});
    }
}

exports.createTweet = async (req, res, next) => {

    const errors = validationResult(req);
    console.log('validation result: ' + errors.isEmpty());
    if (!errors.isEmpty()) {
        return res.status(422).json({errors: errors});
    } //return with error

    let content = req.body.content;
    let tags = req.body.tags;
    try {
        let newPost = await tweetService.createTweet(req.user, content, tags);
        const responseMsg = {message: 'Post created', tweet: newPost};
        console.log(responseMsg);
        return res.status(200).json(responseMsg);
    } catch (err) {
        return res.status(422).json({message: 'cannot save post', error: err});
    }
};

exports.deleteTweet = async (req, res, next) => {
    try {
        const deletedTweet = await tweetService.deleteTweet(req.user, req.params['id']);
        res.status(200).json({message: 'tweet delete successfully', tweet: deletedTweet, status: 200});
    } catch (e) {
        console.log(e);
        res.status(500).json({message: 'unable to delete tweet', status: 500});
    }
};

exports.getMyTimeline = async (req, res, next) => {
    try {
        const timelineTweets = await tweetService.getMyTimeline(req.user);
        res.status(200).json({message: 'success', tweets: timelineTweets, size: timelineTweets.length});
    } catch (e) {
        return res.status(500).json({'message': 'Unable to get timeline tweets. Please try again later.'})
    }
};


exports.createReply = async (req, res, next) => {
    try {
        const reply = await tweetService.createReply(req.user, req.params['id'], req.body.content);
        res.status(201).json({reply: reply})
    } catch (e) {
        return res.status(500).json({message: 'error adding reply to tweet'})
    }
};

exports.deleteReply = async (req, res, next) => {
    try {
        const reply = await tweetService.deleteReply(req.user, req.params['id'], req.params['replyId']);
        res.status(200).json({replyDeleted: reply})
    } catch (e) {
        return res.status(500).json({message: 'error adding reply to tweet'})
    }
};


exports.getMyTweetsCount = async (req, res, next) => {
    try {
        const count = await tweetService.getMyTweetsCount(req.user);
        return res.status(200).json({count: count})
    } catch (e) {
        return res.status(500).json({message: 'error getting my tweets count'})
    }
};

exports.count = async (req, res, next) => {
    try {
        const count = await tweetService.count();
        return res.status(200).json({count: count})
    } catch (e) {
        return res.status(500).json({message: 'error getting my tweets count'})
    }
};

exports.like = async (req, res, next) => {
    try {
        const tweetId = req.params['id'];
        const response = await tweetService.like(req.user, tweetId);
        res.status(200).json({like: response});
    } catch (e) {
        console.log(e);
        return res.status(500).json({message: 'error tweet like'})
    }
};

exports.unlike = async (req, res, next) => {
    try {
        const tweetId = req.params['id'];
        const response = await tweetService.unlike(req.user, tweetId);
        res.status(200).json({like: response});
    } catch (e) {
        return res.status(500).json({message: 'error tweet unlike'})
    }
};

exports.api1 = async (req, res, next) => {
    return res.status(200).json({api: 'response from api 1'});
};

exports.api2 = async (req, res, next) => {
    return res.status(200).json({api: 'response from api 2'});
};

exports.uploadMedia = async (req, res, next) => {
    try {
        const tweetId = req.params['id']
        req.tweetId = tweetId;

        const responseImageUpload = await tweetImageUploader(req, res)
        console.log(responseImageUpload);
        console.log(req.file.tweetId);
        console.log(req.file);
        if (req.file === undefined) {
            return res.status(422).json({message: 'You must select a file for tweet image upload.'});
        } else {
            const response = await tweetService.storeFileDataToTweetObject(req.user, tweetId, req.file);
            return res.status(200).json({
                message: 'File has been uploaded for tweet.' + req.tweetId,
                upload: req.file,
                response: response
            });
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({message: 'Error when trying upload image: ${error}'});
    }
};

exports.getMediaById = async (req, res, next) => {
    const photoId = req.params['id'];
    const gfs = new mongoose.mongo.GridFSBucket(mongoose.connection.db, {bucketName: process.env.TWEET_IMAGES_BUCKET});
    const downStream = gfs.openDownloadStream(new mongoose.Types.ObjectId(photoId));
    return downStream.pipe(res);
};