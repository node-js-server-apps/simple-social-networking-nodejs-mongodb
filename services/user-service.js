const User = require('../models/users')

exports.getUser = async (username) => {
    return User.findOne({username: username}, {password: 0});
};

exports.exists = async (req, res, next) => {
    const targetUserId = req.body.targetUserId;
    try {
        const targetUser = await User.findById(targetUserId, {password: 0});
        if (!targetUser) {
            console.log('user does not exists')
            return res.status(404).json({message: 'user with id ' + targetUserId});
        } else {
            console.log('user does exists')
            req.targetUser = targetUser;
            next();
        }
    } catch (e) {
        return res.status(500).json({message: 'Internal server error for user exists for: ' + targetUserId});
    }
};

exports.count = async () => {
    return User.find({}).count();
};

exports.getSuggestionList = async () => {
    return User.find({}, {password: 0});
};

exports.updateProfileDetails = async (user, id) => {
    return User.updateOne({"_id": user._id}, {$set: {mediaId: id}})
};

exports.findUsersNotIn = async (users, loggedInUser) => {
    const followingUsers = users;
    followingUsers.push(loggedInUser)
    return User.find({_id: {"$nin": followingUsers}});
};
