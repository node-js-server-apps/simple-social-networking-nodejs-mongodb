var express = require('express');
const {body} = require('express-validator')
const tweetsController = require("../controllers/tweets");
const tweetsResolver = require("../middlewares/tweet-resolvers");
const tagResolver = require('../middlewares/tags-resolvers')
const jwtAuth = require('../middlewares/jwt_verify');
var router = express.Router();

const tweetValidator = body('content').trim().isLength({min: 5});

router.get('/', tweetsController.getTweets);
router.get('/total', tweetsController.count);
router.get('/count', jwtAuth.verifyJwt, tweetsController.getMyTweetsCount);
router.get('/me', jwtAuth.verifyJwt, tweetsController.getMyTweets);
router.get('/timeline', jwtAuth.verifyJwt, tweetsController.getMyTimeline);
router.post('/', [tweetValidator], jwtAuth.verifyJwt, tagResolver.resolveTags, tweetsController.createTweet);
router.delete('/:id', jwtAuth.verifyJwt, tweetsResolver.resolveTweetIdAndUser, tweetsController.deleteTweet);
router.post('/:id/reply', jwtAuth.verifyJwt, tweetsResolver.resolveTweetById, tweetsController.createReply);
router.delete('/:id/reply/:replyId', jwtAuth.verifyJwt, tweetsResolver.resolveTweetById, tweetsController.deleteReply);
router.patch('/:id/like', jwtAuth.verifyJwt, tweetsResolver.resolveTweetById, tweetsController.like);
router.patch('/:id/unlike', jwtAuth.verifyJwt, tweetsResolver.resolveTweetById, tweetsController.unlike);
router.get('/api1', tweetsController.api1);
router.get('/api2', tweetsController.api2);
router.post('/upload-media/:id', jwtAuth.verifyJwt, tweetsResolver.resolveTweetById, tweetsController.uploadMedia);
router.get('/media/:id', tweetsController.getMediaById);

module.exports = router;
