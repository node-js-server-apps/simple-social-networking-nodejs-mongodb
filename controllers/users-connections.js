const userConnectionService = require('../services/user-connection-service')
exports.followUser = async (req, res, next) => {
    const followed = await userConnectionService.followUser(req.user, req.targetUser);
    if (followed.error) {
        res.status(500).json(followed);
    } else {
        res.status(200).json(followed);
    }
}

exports.unFollowUser = async (req, res, next) => {
    const followed = await userConnectionService.unFollowUser(req.user, req.targetUser);
    if (followed.error) {
        res.status(500).json(followed);
    } else {
        res.status(200).json(followed);
    }
}

exports.getMyConnections = async (req, res, next) => {
    const myConnections = await userConnectionService.getUserConnectionDetails(req.user);
    res.status(200).json({my_connection: myConnections});
};

exports.isFollowing = async (req, res, next) => {
    try {
        const targetUserId = req.params.id;
        const is_following = await userConnectionService.isFollowing(req.user, targetUserId)
        if (is_following) {
            res.status(200).json({is_following: true});
        } else {
            res.status(200).json({is_following: false});
        }
    } catch (e) {
        res.status(500).json({message: 'Internal Server Error'});
    }
};

exports.getSuggestions = async (req, res, next) => {
    try {
        return res.status(200).json({
            message: 'success',
            users: await userConnectionService.getSuggestions(req.user)
        })
    } catch (e) {
        return res.status(500).json({message: 'error getting user suggestions', users: []});
    }
};


