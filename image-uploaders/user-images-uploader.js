const util = require("util");
const multer = require("multer");
const {GridFsStorage} = require("multer-gridfs-storage");

var storage = new GridFsStorage({
    url: process.env.DB_CONNECTION_STRING,
    options: {useNewUrlParser: true, useUnifiedTopology: true},
    file: (req, file) => {
        const match = ["image/png", "image/jpeg"];

        if (match.indexOf(file.mimetype) === -1) {
            const filename = `${Date.now()}-simple-node-user-image-${file.originalname}`;
            return filename;
        }

        return {
            bucketName: 'user-images',
            filename: `${Date.now()}-simple-node-user-image-${file.originalname}`
        };
    }
});

const uploadFile = multer({storage: storage}).single("file");
const uploadFilesMiddleware = util.promisify(uploadFile);
module.exports = uploadFilesMiddleware;