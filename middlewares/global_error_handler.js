exports.errorHandler = (error, req, res, next) => {
    res.status(500).json({message: 'server encountered error', error: error});
}