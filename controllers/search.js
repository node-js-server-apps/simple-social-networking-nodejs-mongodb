const searchService = require('../services/search-service')


exports.searchTweetsByTags = async (req, res, next) => {
    const tagsQuery = req.query.value;
    if (tagsQuery) {
        let tags = tagsQuery.split(',');
        tags = tags.map(function (x) {
            x = x.toLowerCase();
            x = x.trim();
            return x.replace(/\s/g, "");
        })
        if (!tags) {
            return res.status(500).json({message: 'invalid tags query'});
        } else {
            if (tags.length <= 5) {
                const tweetsByTags = await searchService.searchTweetsByTags(tags);
                return res.status(200).json({
                    tweets: tweetsByTags.tweets,
                    tags: tweetsByTags.tags,
                    size: tweetsByTags.tweetsSize
                });
            } else {
                return res.status(400).json({message: 'too many tags'});
            }
        }
    } else {
        return res.status(500).json({message: 'invalid tags query'});
    }
};

exports.searchTweetsByText = async (req, res, next) => {
    const textQuery = req.body.query;
    console.log('query: ' + textQuery);
    try {
        const response = await searchService.searchTweetsByText(textQuery);
        return res.status(200).json(response);
    } catch (e) {
        console.log(e);
        return res.status(500).json({
            tweets: [],
            text: textQuery,
            tweetsSize: 0
        });
    }
}
