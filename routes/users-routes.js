var express = require('express');
const userController = require("../controllers/users");
const jwtVerify = require("../middlewares/jwt_verify")
var router = express.Router();

/* GET users listing. */
router.get('/', jwtVerify.verifyJwt, userController.getUsers);
router.get('/count', userController.count);
router.get('/me', jwtVerify.verifyJwt, userController.getMe);
router.post('/me', jwtVerify.verifyJwt, userController.updateMe);
router.get('/profile-images', jwtVerify.verifyJwt, userController.profileImages);
router.get('/profile-images/:photoId', userController.profileImagesById);
router.post('/upload-profile-image', jwtVerify.verifyJwt, userController.uploadProfileImage);
router.get('/suggestions', jwtVerify.verifyJwt, userController.suggestions);

module.exports = router;
