const Tweet = require('../models/tweets')
const Tag = require('../models/tags')


exports.searchTweetsByTags = async (tags) => {
    const tagObjects = await Tag.find({name: {"$in": tags}}, {_id: 1, name: 1});
    const tweetsByTags = await Tweet.find({tags: {$in: tagObjects}})
        .populate('tags', '_id name')
        .populate('likes', '_id name')
        .populate('created_by', '_id name username')
        .sort({'createdAt': -1});
    return {
        tweets: tweetsByTags,
        tags: tagObjects,
        tagsSize: tagObjects.length,
        tweetsSize: tweetsByTags.length
    }
};

exports.searchTweetsByText = async (textQuery) => {
    console.log('text search: ' + textQuery);
    const tweetsByText = await Tweet.find({$text: {$search: textQuery}})
        .populate('tags', '_id name')
        .populate('likes', '_id name')
        .populate('created_by', '_id name username')
        .sort({'createdAt': -1});
    return {
        tweets: tweetsByText,
        text: textQuery,
        tweetsSize: tweetsByText.length
    }
};