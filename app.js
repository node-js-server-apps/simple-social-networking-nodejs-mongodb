var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose')
const crypto = require('crypto')//to generate file name
const multer = require('multer')
const GridFsStorage = require('multer-gridfs-storage')
const Grid = require('gridfs-stream')
var cors = require('cors')


var indexRouter = require('./routes/index');
var authRouter = require('./routes/auth-routes');
var usersRouter = require('./routes/users-routes');
var usersConnectionRouter = require('./routes/users-connection-routes');
var tweetsRouter = require('./routes/tweets-routes');
var tagsRouter = require('./routes/tags-routes');
var searchRouter = require('./routes/search-routes');
const dotenv = require("dotenv");
const dbConnection = require('./database/mongo-connection')

console.log(dotenv.config());


// mongoose.connect('mongodb://172.17.0.2:27017/simple-node-js')
//     .then(value => {
//         const gfs = Grid(value.connection.db, mongoose.mongo)
//         gfs.collection(process.env.USER_IMAGES_BUCKET);
//         gfs.collection(process.env.TWEET_IMAGES_BUCKET);
//         console.log('connected to mongo db instance: ');
//     })
//     .catch(error => {
//         console.error(error);
//         console.error('error connecting to mongo database');
//     });

dbConnection.connectToDatabase();
const corsOptions = {
    origin: 'http://localhost:4200',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

const app = express();

app.use(cors(corsOptions));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/auth', authRouter);
app.use('/users', usersRouter);
app.use('/tweets', tweetsRouter);
app.use('/user-connections', usersConnectionRouter);
app.use('/tags', tagsRouter);
app.use('/search', searchRouter);

module.exports = app;
