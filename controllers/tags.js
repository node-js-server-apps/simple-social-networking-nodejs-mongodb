const {validationResult} = require('express-validator')
const tagService = require('../services/tag-service')

exports.getTop5Tags = async (req, res, next) => {
    try {
        const tags = await tagService.getTop5RecentTags();
        res.status(200).json({'tags': tags});
    } catch (e) {
        res.status(500).json({error: 'error getting all tags'});
    }
};

exports.getTags = async (req, res, next) => {
    try {
        const tags = await tagService.getAllTags();
        res.status(200).json({'tags': tags});
    } catch (e) {
        res.status(500).json({error: 'error getting all tags'});
    }
}

exports.createTag = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({errors: errors})
    } //return with error

    const tagName = req.body.tagName;
    try {
        const tag = await tagService.createTag(req.user, tagName);
        res.status(201).json({'tag': tag});
    } catch (e) {
        console.error(e);
        res.status(500).json({error: 'error creating tag: ' + tagName});
    }
};

exports.count = async (req, res) => {
    try {
        const count = await tagService.count();
        return res.status(500).json({count: count});
    } catch (e) {
        return res.status(500).json({message: 'error getting all tags count'});
    }
}
