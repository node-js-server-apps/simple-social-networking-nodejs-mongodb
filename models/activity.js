const mongoose = require('mongoose');
const {SchemaType} = require("mongoose");
const Schema = mongoose.Schema;

//eg: john followed ria
//eg: ria liked john's tweet
const ACTIVITY_TYPE = {
    CREATED_TWEET: "TWEET_CREATE",
    DELETED_TWEET: "TWEET_DELETE",
    LIKED_TWEET: "TWEET_LIKE",
    CREATED_TWEET_COMMENT: "TWEET_CREATE_COMMENT",
    USER_FOLLOWED: "USER_FOLLOWED",
    USER_UNFOLLOWED: "USER_UNFOLLOWED",
    CREATED_TAG: "CREATED_TAG",
};
const activitySchema = new Schema({
    performed_by: {type: Schema.Types.ObjectId, ref: "User", required: true},
    action: {type: ACTIVITY_TYPE, required: true},
    target_user: {type: Schema.Types.ObjectId, ref: "User", required: true},
    target_entity_id: {type: String, required: true}
}, {timestamps: true});

activitySchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        delete ret._id
    }
});

module.exports = mongoose.model('Activity', activitySchema);