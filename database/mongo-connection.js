const mongoose = require("mongoose");
const Grid = require("gridfs-stream");
exports.connectToDatabase = async () => {
    try {
        const mongooseConnection = await mongoose.connect(process.env.DB_CONNECTION_STRING)
        const gfs = Grid(mongooseConnection.connection.db, mongoose.mongo);
        gfs.collection(process.env.USER_IMAGES_BUCKET);
        gfs.collection(process.env.TWEET_IMAGES_BUCKET);
        console.log('connected to mongo db instance from a file ');
        console.log('connected to mongo db profile grid bucket: ' + process.env.USER_IMAGES_BUCKET);
        console.log('connected to mongo db tweet grid bucket: ' + process.env.TWEET_IMAGES_BUCKET);
        return mongooseConnection;
    } catch (e) {
        console.error(e);
        console.error('error connecting to mongo database');
    }
    // .then(value => {
    //     const gfs = Grid(value.connection.db, mongoose.mongo)
    //     gfs.collection(process.env.USER_IMAGES_BUCKET);
    //     gfs.collection(process.env.TWEET_IMAGES_BUCKET);
    //     console.log('connected to mongo db instance: ');
    // })
    // .catch(error => {
    //     console.error(error);
    //     console.error('error connecting to mongo database');
    // });
};
