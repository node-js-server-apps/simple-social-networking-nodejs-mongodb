const Tag = require('../models/tags')

exports.exists = async (name) => {
    return await Tag.findOne({name: name});
}
exports.createTag = async (user, name) => {
    const existingTag = await this.exists(name);
    if (existingTag) {
        console.log('tag ' + name + ' already exists');
        return existingTag;
    } else {
        let newTag = new Tag({name: name, created_by: user})
        return await newTag.save();
    }
};

exports.getTagsForName = async (tags) => {
    return Tag.find({name: {$in: tags}})
};

exports.getAllTags = async () => {
    return Tag.find({}, {created_by: 0, createdAt: 0, updatedAt: 0});
};

exports.getTop5RecentTags = async () => {
    return Tag.find({}, {created_by: 0, createdAt: 0, updatedAt: 0})
        .sort({createdAt: -1})
        .limit(20);
};

exports.count = async () => {
    return Tag.find({}).count();
};