const Activity = require('../models/activity')

exports.saveActivity = async (currentUser, action, targetUser, targetEntityId) => {
    const newActivity = new Activity({
        performed_by: currentUser,
        action: action,
        target_user: targetUser,
        target_entity_id: targetEntityId
    });
    try {
        await newActivity.save();
        return true;
    } catch (e) {
        return false;
    }
};

exports.getMyActivity = async (user) => {
    return Activity.find({performed_by: user}).sort({'createdAt': -1});
};

