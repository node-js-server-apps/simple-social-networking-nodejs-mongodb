const mongoose = require('mongoose');
const {SchemaType} = require("mongoose");
const Schema = mongoose.Schema;

const tweetSchema = new Schema({
    content: {type: String, required: true},
    created_by: {type: Schema.Types.ObjectId, ref: "User", required: true},
    tags: [{type: Schema.Types.ObjectId, ref: "Tag"}],
    media: [{type: String}],
    replies: [{
        user_id: {type: String, required: true},
        name: String,
        username: String,
        content: {type: String, required: true},
        createdAt: {type: Date, default: Date.now},
        updatedAt: {type: Date, default: Date.now}
    }],
    likes: [{type: Schema.Types.ObjectId, ref: "User", required: true}]
}, {timestamps: true});

tweetSchema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        delete ret._id
    }
});
tweetSchema.index({content: 'text'});

module.exports = mongoose.model('Tweet', tweetSchema);