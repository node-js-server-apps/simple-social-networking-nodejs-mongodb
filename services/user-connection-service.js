const UserConnection = require('../models/users-connections')
const constants = require("constants");
const mongoose = require("mongoose");
const userService = require('../services/user-service')


exports.getUserConnection = async (user) => {
    return UserConnection.findOne({user: user});
};

exports.getUserConnectionFollowings = async (user) => {
    return UserConnection.findOne({user: user})
        .populate('followings', '_id name username mediaId');
};

exports.getUserConnectionDetails = async (user) => {
    return UserConnection.findOne({user: user})
        .populate('followers', '_id name username mediaId')
        .populate('followings', '_id name username mediaId');
};

exports.createNewUserConnection = async (user) => {
    const newUserConnection = new UserConnection({user: user, followers: [], followings: []});
    return await newUserConnection.save();
};

exports.followUser = async (currentUser, targetUser) => {
    try {
        const currentUserConnection = await this.getUserConnection(currentUser);
        const targetUserConnection = await this.getUserConnection(targetUser);
        if (currentUserConnection.followings.indexOf(targetUser._id) === -1
            && targetUserConnection.followers.indexOf(currentUser._id)) {
            currentUserConnection.followings.push(targetUser);
            targetUserConnection.followers.push(currentUser);

            await currentUserConnection.save();
            await targetUserConnection.save();

            return {message: 'Followed user ' + targetUser.name};
        } else {
            return {message: 'Your are already following ' + targetUser.name};
        }
    } catch (e) {
        console.error(e);
        return {error: 'Error following user: ' + targetUser.name};
    }

};

exports.unFollowUser = async (currentUser, targetUser) => {
    try {
        const currentUserConnection = await this.getUserConnection(currentUser);
        const targetUserConnection = await this.getUserConnection(targetUser);

        currentUserConnection.followings.remove(targetUser);
        targetUserConnection.followers.remove(currentUser);

        await currentUserConnection.save();
        await targetUserConnection.save();
        return {message: 'Unfollowed user ' + targetUser.name};
    } catch (e) {
        console.error(e);
        return {error: 'Error following user: ' + targetUser.name};
    }
};

exports.isFollowing = async (user, targetUserId) => {
    try {
        const targetUser = await UserConnection.findOne({user: new mongoose.Types.ObjectId(targetUserId)});
        console.log(targetUser);
        if (targetUser) {
            const exists = targetUser.followers.indexOf(user._id);
            return exists === 0;
        } else {
            return false;
        }
    } catch (e) {
        console.log(e);
        return false;
    }
};

exports.getSuggestions = async (user) => {
    try {
        let userConnection = await this.getUserConnectionFollowings(user);
        const followings = userConnection.followings;
        return await userService.findUsersNotIn(followings, user);
    } catch (e) {
        return [];
    }
};
