const User = require('../models/users')
const UserConnectionService = require('../services/user-connection-service')
const {validationResult} = require('express-validator');
const {compareSync} = require("bcrypt");
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()

const {authLogger} = require('../logger/app-logger').logger
const fs = require('fs');

var privateKey = fs.readFileSync('./keys/jwtRS256.key', 'utf8');

exports.login = async (req, res, next) => {
    const validationError = validationResult(req);
    if (!validationError.isEmpty()) {
        res.status(422).json({'error': validationError.array()})
    } else {
        const username = req.body.username;
        const password = req.body.password;
        try {
            const user = await User.findOne({username: username});
            if (compareSync(password, user.password)) {
                const token = await jwt.sign({id: user._id, username: user.username, name: user.name},
                    {key: privateKey, passphrase: 'qawsed'},
                    {algorithm: 'RS256', expiresIn: '3 days'}
                );

                const existingUserConnection = await UserConnectionService.getUserConnection(user);
                if (!existingUserConnection) {
                    await UserConnectionService.createNewUserConnection(user);
                    console.log('UserConnection was not present for: ' + user.username);
                } else {
                    console.log('UserConnection Already exists');
                }
                console.log(existingUserConnection);
                authLogger.info({type: 'login', user: user.name});
                res.status(200).json({
                    id: user._id,
                    user: user.name,
                    username: user.username,
                    message: 'login successful',
                    token: token
                });
            } else {
                res.status(401).json({'message': 'Bad Credentials'})
            }
        } catch (err) {
            console.error(err);
            res.status(401).json({'message': 'Bad Credentials'})
        }
    }
};

exports.register = async (req, res, next) => {
    const validationError = validationResult(req);
    if (!validationError.isEmpty()) {
        return res.status(422).json({'error': validationError.array()})
    }
    const username = req.body.username;
    const password = req.body.password;
    const name = req.body.name;
    try {
        const existingUser = await User.exists({username: username});
        if (!existingUser) {
            const newUser = new User({name: name, username: username, password: password});
            await newUser.save();
            const existingUserConnection = await UserConnectionService.getUserConnection(newUser);
            console.log('existing user connection');
            console.log(existingUserConnection);
            if (!existingUserConnection) {
                await UserConnectionService.createNewUserConnection(newUser);
                console.log('UserConnection was not present for: ' + newUser.username);
            } else {
                console.log('UserConnection Already exists');
            }
            const token = await jwt.sign({
                id: newUser._id,
                username: newUser.username,
                name: newUser.name
            }, process.env.TOKEN_SECRET, {expiresIn: '7200s'});
            res.status(200).json({
                id: newUser._id,
                user: newUser.name,
                username: newUser.username,
                message: 'login successful',
                token: token
            });
        } else {
            res.status(400).json({message: 'User already exists'});
        }
    } catch (err) {
        console.error(err);
        res.status(422).json({message: 'Error while registering new user'});
    }

};
