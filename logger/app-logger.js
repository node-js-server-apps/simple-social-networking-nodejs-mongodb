const winston = require('winston');
require('winston-mongodb');
// const logServer = 'mongodb://172.17.0.2:27017/logs';
const defaultLoggerConfig = {
    format: winston.format.json(),
    'transports': [
        new winston.transports.Console(),
        new winston.transports.MongoDB({
            level: 'error',
            //mongo database connection link
            db: process.env.DB_CONNECTION_STRING,
            options: {
                useUnifiedTopology: true
            },
            // A collection to save json formatted logs
            collection: 'server_logs',
            format: winston.format.combine(
                winston.format.timestamp(),
                // Convert logs to a json format
                winston.format.json())
        })
    ]
};

const authLoggerConfig = {
    format: winston.format.json(),
    'transports': [
        new winston.transports.Console(),
        new winston.transports.MongoDB({
            // level: 'error',
            //mongo database connection link
            db: process.env.DB_CONNECTION_STRING,
            options: {
                useUnifiedTopology: true
            },
            // A collection to save json formatted logs
            collection: 'auth_logs',
            format: winston.format.combine(
                winston.format.timestamp(),
                // Convert logs to a json format
                winston.format.json())
        })
    ]
};

const searchLoggerConfig = {
    format: winston.format.json(),
    'transports': [
        new winston.transports.Console(),
        new winston.transports.MongoDB({
            // level: 'error',
            //mongo database connection link
            db: process.env.DB_CONNECTION_STRING,
            options: {
                useUnifiedTopology: true
            },
            // A collection to save json formatted logs
            collection: 'search_logs',
            format: winston.format.combine(
                winston.format.timestamp(),
                // Convert logs to a json format
                winston.format.json())
        })
    ]
};

const tagsLoggerConfig = {
    format: winston.format.json(),
    'transports': [
        new winston.transports.Console(),
        new winston.transports.MongoDB({
            // level: 'error',
            //mongo database connection link
            db: process.env.DB_CONNECTION_STRING,
            options: {
                useUnifiedTopology: true
            },
            // A collection to save json formatted logs
            collection: 'tags_logs',
            format: winston.format.combine(
                winston.format.timestamp(),
                // Convert logs to a json format
                winston.format.json())
        })
    ]
};

const tweetLoggerConfig = {
    format: winston.format.json(),
    'transports': [
        new winston.transports.Console(),
        new winston.transports.MongoDB({
            // level: 'error',
            //mongo database connection link
            db: process.env.DB_CONNECTION_STRING,
            options: {
                useUnifiedTopology: true
            },
            // A collection to save json formatted logs
            collection: 'tweet_logs',
            format: winston.format.combine(
                winston.format.timestamp(),
                // Convert logs to a json format
                winston.format.json())
        })
    ]
};


const defaultLogger = winston.createLogger(defaultLoggerConfig);
const authLogger = winston.createLogger(authLoggerConfig);
const searchLogger = winston.createLogger(searchLoggerConfig);
const tagLogger = winston.createLogger(tagsLoggerConfig);
const tweetLogger = winston.createLogger(tweetLoggerConfig);

exports.logger = {defaultLogger, authLogger, searchLogger, tagLogger, tweetLogger};