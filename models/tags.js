const mongoose = require('mongoose');
const {SchemaType} = require("mongoose");
const Schema = mongoose.Schema;

const tagSchema = new Schema({
    name: {type: String, required: true},
    created_by: {type: Schema.Types.ObjectId, ref: "User", required: true},
    tweets: {type: Schema.Types.ObjectId, ref: "Tweet"}
}, {timestamps: true});

tagSchema.set('toJSON', {
    virtuals: true,
    versionKey:false,
    transform: function (doc, ret) {   delete ret._id  }
});

module.exports = mongoose.model('Tag', tagSchema);