var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    const messsage = req.query.message;
    console.log('message: ' + messsage);
    let title = 'Express';
    if (messsage) {
        title.append(' ').append(messsage);
    }
    res.render('index', {title: title});
});

module.exports = router;
