[
  {
    '$unwind': {
      'path': '$tags',
      'preserveNullAndEmptyArrays': true
    }
  }, {
  '$lookup': {
    'from': 'tags',
    'localField': 'tags',
    'foreignField': '_id',
    'as': 'tweet_tags'
  }
}, {
  '$unwind': {
    'path': '$tweet_tags',
    'preserveNullAndEmptyArrays': true
  }
}, {
  '$group': {
    '_id': '$_id',
    'tags': {
      '$push': '$tweet_tags'
    }
  }
}
]