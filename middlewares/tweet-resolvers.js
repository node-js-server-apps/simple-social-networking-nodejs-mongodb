const tweetService = require('../services/tweet-service')


exports.resolveTweetIdAndUser = async (req, res, next) => {
    try {
        const tweetId = req.params['id'];
        const tweet = await tweetService.findOne(req.user, tweetId);
        if (tweet) {
            next()
        } else {
            return res.status(400).json({message: 'tweet not found'});
        }
    } catch (e) {
        return res.status(400).json({message: 'tweet not found'});
    }

};

exports.resolveTweetById  = async (req, res, next) => {
    try {
        const tweetId = req.params['id'];
        const tweet = await tweetService.findOneById(tweetId);
        console.log(tweet);
        if (tweet) {
            next()
        } else {
            return res.status(400).json({message: 'tweet not found'});
        }
    } catch (e) {
        return res.status(500).json({message: 'error uploading tweet image'});
    }
};